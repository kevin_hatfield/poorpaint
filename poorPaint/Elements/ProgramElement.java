/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics 
 *   identified for development of Project 2 in Object-Oriented Design I SSE 
 *   550 Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Elements;

import java.util.ArrayList;

/**
 * Class ProgramElement
 * 
 * Class is skeleton for holding all program drawing objects.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 */
public class ProgramElement {
    /* Variables */
	private static ArrayList<DrawObject> myList = new ArrayList<DrawObject>();
	
	/**
	 * Instantiate a new ProgramElement object.
	 * Default constructor.
	 */
	public ProgramElement() {}

	/**
	 * Add a drawing object to the list.
	 */
	public boolean addElement(DrawObject whichOne) {
		return getMyList().add(whichOne);
	}

	/**
	 * Remove a drawing object from the list.
	 */
	public boolean deleteElement(DrawObject whichOne) {
		return getMyList().remove(whichOne);
	}

	/**
	 * Empty the list.
	 */
	public boolean deleteAll() {
		return getMyList().removeAll(getMyList());
	}

	/**
	 * Provide the element count.
	 */
	public int countElements() {
		return getMyList().size();
	}

	/**
	 * Return a single element.
	 */
	public DrawObject returnElement(int whichOne) {
		return getMyList().get(whichOne);
	}

	public static ArrayList<DrawObject> getMyList() {
		return myList;
	}

	public static void setMyList(ArrayList<DrawObject> myList) {
		ProgramElement.myList = myList;
	}
}