/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Elements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JComponent;

import poorPaint.Controls.DrawControl;
import poorPaint.Controls.SystemControl;
import poorPaint.Controls.ToolControl;

/**
 * Class MyCanvas
 * 
 * Provides the drawing canvas for the draw panel.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 */

//public class MyCanvas extends JPanel {

/* changed to JComponent as JPanel instance exists in DrawControl */

public class MyCanvas extends JComponent {
	
	private static final long serialVersionUID = (long) SystemControl.appInfo.
			VERSION.value();

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		/* try block: ensure new graphics2D instance destroyed */
		
		try
		{		
			ProgramElement myList = new ProgramElement();

			int theCount = myList.countElements();

			DrawObject myObject = new DrawObject(0, 0, 0, 0, 0, Color.white, 
					false, 1, new ArrayList<Point>());

			for (int x=0; x<theCount; x++) {
				myObject = myList.returnElement(x);
				g2d.setColor(myObject.myColor);

				int myBrushStroke = myObject.myBrushSize;
				
				g2d.setStroke(new BasicStroke(myBrushStroke));
				
				Point startCoordinates = new Point(myObject.myStartX,
						myObject.myStartY);

				Point endCoordinates = new Point(myObject.myEndX, 
						myObject.myEndY);

				Rectangle shapeBound = DrawControl.rectangleCorrectedPosition(
						startCoordinates, 
						endCoordinates);

				if(myObject.meSquare)
				{
					shapeBound = DrawControl.squareCorrectedPosition(
							startCoordinates, 
							endCoordinates);

				}//if

				if (myObject.myType==ToolControl.RECTANGLE_NUM) {

					g2d.draw(shapeBound);

				} else if ( myObject.myType==ToolControl.LINE_NUM ) {

					g2d.drawLine(myObject.myStartX, myObject.myStartY, 
							myObject.myEndX, myObject.myEndY);

				} else if ( myObject.myType==ToolControl.CIRCLE_NUM ) {

					g2d.drawOval(shapeBound.x, shapeBound.y, 
							shapeBound.width, shapeBound.height);

				} else {	//freehand default

					if( myObject.myFreeHandPts.size() > 1 )
					{
						for(int ct=0; ct<myObject.myFreeHandPts.size()-1; ct++)
						{
							g2d.drawLine(myObject.myFreeHandPts.get(ct).x,
									myObject.myFreeHandPts.get(ct).y, 
									myObject.myFreeHandPts.get(ct+1).x, 
									myObject.myFreeHandPts.get(ct+1).y);

						}//for

					}//if

				}//if

			}//for
		
		}
		finally
		{
			g2d.dispose();	//manually destroy as each canvas instance creates
		
		}//finally
		
	}//method
	
}//class