/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Elements;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import poorPaint.Controls.SystemControl;

/**
 * Class DrawObject
 * 
 * Provides the template for a drawing object.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 */
public class DrawObject implements Serializable {

	private static final long serialVersionUID = (long) SystemControl.appInfo.
			VERSION.value();
	
	/* Variables */
	int myStartX;
	int myEndX;
	int myStartY;
	int myEndY;
	int myType;
	Color myColor;
	
	/* ability to draw squares, circles */
	
	boolean meSquare = false;

	int myBrushSize = 1;
	
	/* ability to draw freehand */
	
	List<Point> myFreeHandPts = new ArrayList<Point>();
	
	/**
	 * Class initiator designed to set initial variable values.
	 */
	public DrawObject(int sx, int ex, int sy, int ey, int t, Color c, 
			boolean amSquare, int brushSizeIn, List<Point> freeHandPtsIn) {
		myStartX = sx;
		myEndX = ex;
		myStartY = sy;
		myEndY = ey;
		myType = t;
		myColor = c;
		
		meSquare = amSquare;
		
		myFreeHandPts = freeHandPtsIn;
		
		myBrushSize = brushSizeIn;
	}
}