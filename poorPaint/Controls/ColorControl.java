/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import poorPaint.Interfaces.IColor;

/**
 * Class ColorControl
 * 
 * Provides the color panel for the PoorPaint program.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @see java.swing.JPanel
 */
public class ColorControl implements IColor {
	/* Constants */
	//public final Color RED_NUM = Color.RED;
	//public final Color BLUE_NUM = Color.BLUE;
	
    /* Variables */
	public static Color selectedColor = Color.BLACK; 
	public static JLabel selColorLbl = new JLabel();

	/**
	 * Empty class initiator which does not reset variables.
	 */
	public ColorControl () {}
	
	/**
	 * Class initiator designed to set initial variable values.
	 */
	public ColorControl (JPanel whichOne) {
			
		JPanel btmPnl = new JPanel();
		btmPnl.setLayout(new BoxLayout(btmPnl,BoxLayout.Y_AXIS));
				
		btmPnl.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JPanel colorsAssortmentPanelTop = new JPanel(
				new FlowLayout(FlowLayout.LEADING,0,0));

		JPanel colorsAssortmentPanelBtm = new JPanel(
				new FlowLayout(FlowLayout.LEADING,0,0));

		colorsAssortmentPanelTop.setAlignmentX(Component.LEFT_ALIGNMENT);
		colorsAssortmentPanelBtm.setAlignmentX(Component.LEFT_ALIGNMENT);
		colorsAssortmentPanelBtm.setAlignmentY(Component.TOP_ALIGNMENT);

		/* area showing chosen color */
		
		selectedColor = Color.BLACK;
		
		selColorLbl.setBackground(Color.BLACK);
		selColorLbl.setOpaque(true);
		selColorLbl.setName("Selected color");
		
		selColorLbl.setPreferredSize(new Dimension(35,25));

		selColorLbl.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createLineBorder(Color.WHITE,7),
						BorderFactory.createDashedBorder(Color.DARK_GRAY, 1, 
								1.2f, 1.4f, true) 
				));
		
		selColorLbl.setAlignmentY(Component.TOP_ALIGNMENT);

		colorsAssortmentPanelTop.add(Box.createRigidArea(
				new Dimension(3, 1)));

		colorsAssortmentPanelTop.add(selColorLbl);

		colorsAssortmentPanelTop.add(Box.createRigidArea(
				new Dimension(3, 1)));

		colorsAssortmentPanelBtm.add(Box.createRigidArea(
				new Dimension(41, 1)));

		/* alpha sorted colors, two rows */
		
		SortedMap<String,Color> supportedColorsTop = 
				new TreeMap<String,Color>();
		
		SortedMap<String,Color> supportedColorsBtm = 
				new TreeMap<String,Color>();
		
		supportedColorsTop.put("Black", Color.BLACK);
		supportedColorsTop.put("DarkGray", Color.DARK_GRAY);
		supportedColorsTop.put("Red", Color.RED);
		supportedColorsTop.put("Orange", Color.ORANGE);
		supportedColorsTop.put("Yellow", Color.YELLOW);
		supportedColorsTop.put("Green", Color.GREEN);
		supportedColorsTop.put("Cyan", Color.CYAN);
		supportedColorsTop.put("Blue", Color.BLUE);
		supportedColorsTop.put("Pink", Color.PINK);
		
		supportedColorsBtm.put("White", Color.WHITE);
		supportedColorsBtm.put("LightGray", Color.LIGHT_GRAY);
		supportedColorsBtm.put("Magenta", Color.MAGENTA);		
		
		for(String colorNme : supportedColorsTop.keySet())
		{
			colorsAssortmentPanelTop.add(createColor(colorNme,
					supportedColorsTop.get(colorNme)));
		
		}//for

		for(String colorNme : supportedColorsBtm.keySet())
		{
			colorsAssortmentPanelBtm.add(createColor(colorNme,
					supportedColorsBtm.get(colorNme)));
		
		}//for

		btmPnl.setOpaque(false);
		colorsAssortmentPanelTop.setOpaque(false);
		colorsAssortmentPanelBtm.setOpaque(false);
		
		btmPnl.add(colorsAssortmentPanelTop);
		
		btmPnl.add(colorsAssortmentPanelBtm);
		
		whichOne.add(btmPnl);		

	}

	@Override
	public JButton createColor(String myName, Color colorIn) 
	{
		JButton myButton = new JButton();
		
		Dimension colorBtnMin = new Dimension(10,10);
		Dimension colorBtnMed = new Dimension(20,20);
		Dimension colorBtnMax = new Dimension(30,30);
		
		myButton.setName(myName);
		myButton.setSize(colorBtnMed);
		myButton.setPreferredSize(colorBtnMed);
		
		myButton.setMinimumSize(colorBtnMin);
		myButton.setMaximumSize(colorBtnMax);
		
		myButton.setBackground(colorIn);
		
		myButton.setToolTipText(myName);
		
		myButton.addActionListener(new ColorListener(colorIn));
		myButton.setHorizontalAlignment(SwingConstants.CENTER);
		
		return myButton;
	}

	@Override
	public void selectColor(Color myColor) {
		selectedColor = myColor;
	}

	/**
	 * Class ColorListener
	 * 
	 * Inner class providing the listening service for the toolbar.
	 * 
	 * @version 1.0 March 18 2013
	 * @author Shane Milam
	 */
	public class ColorListener implements ActionListener {
		public Color myColor;
		
		/**
		 * Initiate color listening class and link the button to the listener
		 */
		public ColorListener (Color colorCodeIn) {
			selectedColor = colorCodeIn;
			myColor = selectedColor;
						
		}
		
		public void actionPerformed(ActionEvent myEvent) {
			selectColor (myColor);
			
			selColorLbl.setBackground(myColor);

		}
	}
	

	@Override
	public void setColor(Color myColor) {
		// TODO Auto-generated method stub	
	}
}