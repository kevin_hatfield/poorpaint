/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Controls;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JPanel;

import poorPaint.Elements.DrawObject;
import poorPaint.Elements.MyCanvas;
import poorPaint.Elements.ProgramElement;
import poorPaint.Interfaces.IDraw;

/**
 * Class ToolControl
 * 
 * Provides the drawing functions for the draw panel.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @param myPanel The actual graphic panel for AppPanel.
 * @see java.swing.JPanel
 */
public class DrawControl implements IDraw {
	public static JPanel myPanel;
    public static int startX;
    public static int startY;
    public static int endX;
    public static int endY;
    public static int drawType;
    public static Color drawColor;
    public ProgramElement myElement;
    
    /** Boolean  User presses shift during shape drawing for square, circle */
    
    public static Boolean shiftKeyActive = false;
    
    /** Point  Positions touched using using Freehand tool */
    
    public static List<Point> freeHandPoints = new ArrayList<Point>();
    
    /** 
     * Boolean  T if recursion done to paintAll repainting method.<ul>
     * <li>T - clearing completed of previous bounding box as mouse is dragged
     * <li>F - previous mouse drag temporary bounding box drawn, not flushed 
     * away through paintAll repainting operation of Panel graphics
     */
    
    public static Boolean repaintComplete = false;

	/**
	 * Empty class initiator which does not reset variables.
	 */
	public DrawControl () {}
	
	/**
	 * Class initiator designed to set initial variable values.
	 */
	public DrawControl (JPanel whichOne) {
		
		/* Add mouse movement, tracking needed for bounding box */
		
		//whichOne.addMouseListener(new DrawListener());
		
		DrawListener drawDetection = new DrawListener();
		whichOne.addMouseListener(drawDetection);
		whichOne.addMouseMotionListener(drawDetection);
		whichOne.addKeyListener(drawDetection);
		
		myPanel = whichOne;
		
		myElement = new ProgramElement();

		/* allow user to hold shift key for square, circle */
		
		whichOne.setFocusable(true);
		
		freeHandPoints = new ArrayList<Point>();
	}

	/**
	 * Class MouseListener
	 * 
	 * Inner class providing the mouse listening service for the drawing panel.
	 * 
	 * @version 1.0 March 18 2013
	 * @author Shane Milam
	 */
	public class DrawListener extends KeyAdapter implements MouseListener, 
		MouseMotionListener 
	{
		public int myTool;

		@Override
		public void mousePressed(MouseEvent arg0) {
			startX = arg0.getX();
			startY = arg0.getY();
			
			myPanel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
			
			/* allow user to use keyboard for shift key */

			myPanel.requestFocus(true);
			
		}//method

		@Override
		public void mouseReleased(MouseEvent arg0) {

			endX = arg0.getX();
			endY = arg0.getY();
			
			myPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			
			drawSomething(new Point(startX,startY),new Point(endX,endY));
			
		}//method

		
		/**
		 * Relocate draw shape/line logic from mouseReleased.<p>
		 * 
		 * <ul>
		 * <li>Consolidate arguments into parameter objects, 4 int : 2 Points
		 * <li>Support drawing without X,Y instance var's as argument values 
		 * </ul>
		 * 
		 * @param startCoord  origin Point in drawing area
		 * @param endCoord  end Point in drawing area
		 */
		
		private void drawSomething(final Point startCoord, 
				final Point	endCoord)
		{						
			/* Prevent drawing off canvas, stop at maximum size allowed */
			
			if( !myPanel.contains(endCoord) && myPanel.contains(startCoord))
			{ 
				return;
				
			}//if
				
			final int drawType = ToolControl.selectedTool;
			final Color drawColor = ColorControl.selectedColor;
			
			/* old */
			
			//DrawObject newThing = 
			//new DrawObject(startX, endX, startY, endY, drawType, drawColor);
			
			/* replaced by */
			
			DrawObject newThing = new DrawObject( startCoord.x, endCoord.x, 
					startCoord.y, endCoord.y, drawType, drawColor, 
					shiftKeyActive, (int) ToolControl.brushSizeCtrl.getValue(),
					Collections.unmodifiableList(new ArrayList<Point>()));

			/* add additional points collection for freehand drawing */
			
			if(ToolControl.selectedTool==ToolControl.FREEHAND_NUM)
			{
				/* collections copy seems broken, doing manually */
				
				List<Point> listOfPts = new ArrayList<Point>();
				
				for(int cx=0; cx<freeHandPoints.size(); cx++)
				{
					listOfPts.add( freeHandPoints.get(cx) );
					
				}//for
				
				newThing = new DrawObject( startCoord.x, endCoord.x, 
						startCoord.y, endCoord.y, drawType, drawColor, 
						shiftKeyActive,  (int) ToolControl.brushSizeCtrl.
						getValue(), listOfPts);
	
			}//if


			final ProgramElement newElement = new ProgramElement();
			newElement.addElement(newThing);
			
			myPanel.repaint();
			
			drawIt();

			/* JPanel refresh after adding a myCanvas */
			
			//myPanel.setVisible(false);
			//myPanel.setVisible(true);
			myPanel.revalidate();

			freeHandPoints.clear();
			
		}//method

		
		/** TODO Use combined mouse, keyboard adapter to avoid empty methods */
		
		/* MouseListener */
		
		
		@Override
		public void mouseClicked(MouseEvent arg0) {}
		
		
		@Override
		public void mouseEntered(MouseEvent arg0) {}
		
		
		@Override
		public void mouseExited(MouseEvent arg0) {}

		
		/* MouseMotionListener */
		
		
		@Override
		public void mouseMoved(MouseEvent arg0) {}

		
		/**
		 * Mouse movement with button depressed.
		 *
		 * @param scurry  MouseEvent Mouse coordinates
		 */
		
		@Override
		public void mouseDragged(final MouseEvent scurry) 
		{
			/* Prevent drawing off canvas */

			final Graphics2D sketchGraphics = (Graphics2D) myPanel.
					getGraphics();

			
			Integer brushSize = (Integer) ToolControl.brushSizeCtrl.getValue();

			sketchGraphics.setStroke(new BasicStroke(brushSize));
							
			
			if( !myPanel.contains(scurry.getPoint()) )
			{
				myPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				
				myPanel.paintAll(sketchGraphics);

				repaintComplete = true;

				sketchGraphics.dispose();
				
				return;
				
			}//if
			
			myPanel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
			
			sketchGraphics.setColor(ColorControl.selectedColor);

			Rectangle sketchArea = rectangleCorrectedPosition(
					new Point(startX, startY), scurry.getPoint());

			
			if(shiftKeyActive) sketchArea = squareCorrectedPosition(
					new Point(startX, startY), scurry.getPoint());
			
			
			if( !repaintComplete )
			{
				myPanel.paintAll(sketchGraphics);
				
				repaintComplete = true;
				
				/* 
				 * recursive call required for preventing repaint erasure 
				 * of most recent drawing
				 */
				
				mouseDragged(scurry);
				
			}//if
			
			repaintComplete = false;
			

			switch(ToolControl.selectedTool)
			{
			
			case(ToolControl.LINE_NUM):
			{
				sketchGraphics.drawLine(startX, startY, scurry.getX(), 
						scurry.getY());
				break;
				
			}//case


			case(ToolControl.RECTANGLE_NUM):
			{
				sketchGraphics.draw(sketchArea);

				sketchGraphics.finalize();
				
				break;
				
			}//case
			
			
			case(ToolControl.CIRCLE_NUM):
			{
				sketchGraphics.drawOval(sketchArea.x, sketchArea.y, 
						sketchArea.width, sketchArea.height);
				
				break;
				
			}//case
			
			
			case(ToolControl.FREEHAND_NUM):
			{
				freeHandPoints.add(scurry.getPoint());
				
				if(freeHandPoints.size() > 1)
				{
					for(int ct=0; ct < freeHandPoints.size()-1; ct++ )
					{
						Point currPos = freeHandPoints.get(ct);
						Point nextPos = freeHandPoints.get(ct+1);
						
						sketchGraphics.drawLine(currPos.x, currPos.y, 
								nextPos.x, nextPos.y);
						
					}//for
					
				}//if
				
				break;
				
			}//case
			
			default: System.out.println("Unknown drawing shape/tool "+
					"specified: " + ToolControl.selectedTool);
				
			}//switch
			
			sketchGraphics.dispose();
			
		}//method


		@Override
		public void keyPressed(KeyEvent keyDwnEvt) 
		{
			if( !shiftKeyActive && keyDwnEvt.getKeyCode() == KeyEvent.
					VK_SHIFT ) 
			{
				shiftKeyActive = true;
				
				if(ToolControl.selectedTool==ToolControl.RECTANGLE_NUM)
				{
					ToolControl.selToolLbl.setText("Square");

				}
				else if(ToolControl.selectedTool==ToolControl.CIRCLE_NUM)
				{
					ToolControl.selToolLbl.setText("Circle");
					
				}//if
				
			}//if
			
		}//method

		
		@Override
		public void keyReleased(KeyEvent keyUpEvt) 
		{
			if( keyUpEvt.getKeyCode() == KeyEvent.VK_SHIFT ) 
			{
				shiftKeyActive = false;

				if(ToolControl.selectedTool==ToolControl.RECTANGLE_NUM || 
						ToolControl.selectedTool == ToolControl.CIRCLE_NUM)
				{
					ToolControl.selToolLbl.setText(new ToolControl().
							returnName());

				}//if
				
			}//if
			
		}//method
		
	}//inner class

	
	/**
	 * Create rectangle using two points, start and ending diagonal.
	 * 
	 * Supply correct X,Y,width,height to Rectangle constructor<p>
	 * Correct / transpose rectangle as needed for negative coordinates
	 * 
	 * @param startPt  Point origin
	 * @param endPt  Point farthest point, corner of rectangle from origin
	 * @return  Rectangle drawn in correct position
	 */
	
	public static Rectangle rectangleCorrectedPosition(final Point startPt, 
			final Point endPt)
	{
		int width, height;
		int correctX, correctY;

		if (endPt.x <= startPt.x) 
		{
			correctX = endPt.x;
			width = startPt.x - correctX;
			
		} 
		else 
		{
			correctX = startPt.x;
			width = endPt.x - correctX;
			
		}//if

		if (endPt.y <= startPt.y) 
		{
			correctY = endPt.y;
			height = startPt.y - correctY;
			
		} 
		else 
		{
			correctY = startPt.y;
			height = endPt.y - correctY;
			
		}//if
		
		return new Rectangle(correctX, correctY, width, height);
		
	}//method

	
	/**
	 * Create square in correct position, transposed for negative coordinates.
	 * 
	 * @param startPt  Point origin
	 * @param endPt  Point farthest along diagonal
	 * @return Rectangle  Square dimensions using shortest side
	 */
	
	public static Rectangle squareCorrectedPosition(final Point startPt,
			final Point endPt)
	{
		final Rectangle rectangleIrreg = 
				rectangleCorrectedPosition(startPt,endPt);

		final int width  = rectangleIrreg.width;
		final int height = rectangleIrreg.height;
		
		final int x = rectangleIrreg.x;
		final int y = rectangleIrreg.y;
		
		return width < height ?	new Rectangle(x, y, width, width)
		
					: new Rectangle(x, y, height, height);
			
	}//method
	
	
	@Override
	public void drawIt() {
		myPanel.removeAll();
		myPanel.add(new MyCanvas());
	}
		
}//class