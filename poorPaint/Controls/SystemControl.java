/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Controls;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

import poorPaint.PoorPaint;
import poorPaint.Elements.MyCanvas;
import poorPaint.Elements.DrawObject;
import poorPaint.Elements.ProgramElement;

/**
 * Class SystemControl
 * 
 * Provides the system control panel for the PoorPaint program.
 * 
 * Uses default provided constructor.  Does not initialize values.  Depends 
 * upon application generating first call in panel function for variable 
 * initialization.
 * 
 * @Retention(RetentionPolicy.RUNTIME)
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @see java.swing.JPanel
 */
public class SystemControl{
	
	/* Constants */
	public static final int SAVE_NUM = 0;
	public static final int LOAD_NUM = 1;
	public static final int ERASE_NUM = 2;
	public static final int QUIT_NUM = 3;
	
	/** Icon graphics for System function buttons, App Icon. */
	
	public static enum icons
	{
		/** Menu icon file names */
		
		fileNames ("save.jpeg","open.jpeg","eraser.jpeg","quit.jpeg"),
				
		/** Top level icon file name, icon for the PoorPaint application */
		
		appIconFileName ("paint.jpeg"),
		
		/** Remote copy of icon img files used by PoorPaint */
		
		iconAuxillaryArchive ("http://gain.mercer.edu/downloads/");
	
		Map<String,Integer> fileNamesMap = new HashMap<String,Integer>();
		
		int[] buttonCodes = {SAVE_NUM,LOAD_NUM,ERASE_NUM,QUIT_NUM};
		
		String iconFileName = "";
		
		icons(String... fileNamesIn) 
		{ 
			if(fileNamesIn.length > 1)
			{
				for(int ct=0; ct< fileNamesIn.length; ct++)
				{
					fileNamesMap.put(PoorPaint.iconSubFolder+fileNamesIn[ct],
							buttonCodes[ct]);
				
				}//for
			
			}
			else
			{
				iconFileName = fileNamesIn[0];
				
			}//if
			
		}//constructor
		
		
		/** @return Map  filenames and constant codes for icons, tools */
		
		Map<String,Integer> valueList()
		{
			return Collections.unmodifiableMap(fileNamesMap);
			
		}//method
		
		
		/** @return  String for single enum entry; ex. icon for application */
		
		public String val(){ return iconFileName; }
		
		
		/**
		 * Look up icon file name for given tool number.
		 * 
		 * @param toolCodeIn  Integer of tool number
		 * @return String  file name of icon graphic for given tool number 
		 */
		
		public String iconFileName(Integer toolCodeIn)
		{
			if( fileNamesMap.containsValue(toolCodeIn) )
			{
				for(String fileNmIn : fileNamesMap.keySet() )
				{
					if( fileNamesMap.get(fileNmIn) == toolCodeIn )
						return fileNmIn;
					
				}//for
				
			}//if
			
			return "Not found";
			
		}//method
		
	}//enum

	
	/** 
	 * Application information for entire PoorPaint program.<p>
	 * 
	 * Version is used by multiple classes for serialization: saved objects
	 */
	
	public static enum appInfo
	{
		VERSION (1.1f);
		
		private float verNum;
		
		appInfo(float verNumIn){ verNum = verNumIn; }
		
		public float value(){ return verNum; }
		
	}//enum
	
	
    /* Variables */
	private static ArrayList<JButton> systemButtons = new ArrayList<JButton>();
	public static int selectedSystem; 

	/**
	 * Class initiator designed to set initial variable values.
	 */
	public SystemControl (JPanel whichOne) {
		
		/* moved to AppPanel */
		
	}
	
	
	/** No arg constructor for methods resistant to static implementation */
	
	public SystemControl () {}
		
	
	/**
	 * Returns the name of currently selected system function.
	 */
	public String returnName() {
		String myName;

		switch(selectedSystem) {
		case SAVE_NUM:
			myName = "Save";
			break;
		case LOAD_NUM:
			myName = "Load";
			break;
		case ERASE_NUM:
			myName = "Erase";
			break;
		case QUIT_NUM:
			myName = "Quit";
			break;
        default:
        	myName = "";
        	break;
		}		
		return myName;
	}

	/**
	 * Create an initial system function button.
	 * 
	 * @deprecated Menu, icons moved to AppPanel
	 */
	public void createSystem(String myName, int myFunction) {
		JButton myButton = new JButton(myName);
		selectSystem(myFunction);
		myButton.addActionListener(new SystemListener());
		myButton.setHorizontalTextPosition(SwingConstants.CENTER);
		myButton.setHorizontalAlignment(SwingConstants.CENTER);
		systemButtons.add(myFunction, myButton);		
	}

	/**
	 * Set the currently selected system function.
	 */
	public void selectSystem(int mySystem) {
		selectedSystem = mySystem;
	}

	/**
	 * Class SystemListener
	 * 
	 * Inner class providing the listening service for the toolbar.
	 * 
	 * @version 1.0 March 18 2013
	 * @author Shane Milam
	 */
	public class SystemListener implements ActionListener {
		public int mySystem;
		
		/**
		 * Initiate system item listening class and link the button to the 
		 * listener
		 */
		public SystemListener () {
			mySystem = selectedSystem;
		}
		
		@Override
		public void actionPerformed(ActionEvent myEvent) {
			selectSystem (mySystem);

			switch(mySystem) {
			case SAVE_NUM:
				SaveSelection();
				break;
			case LOAD_NUM:
				LoadSelection();
				break;
			case ERASE_NUM:
				ClearScreen();
				break;
			case QUIT_NUM:
				ExitPoorPaint();
				break;
			}			
		}
	}

	
	/**
	 * Open file selection dialog and pass result to second step of file open.
	 */
	public static void LoadSelection () {
		JFileChooser fileOpen = new JFileChooser();
		fileOpen.showOpenDialog(new JFrame("Select File"));
		
		/** TODO recall last used file name and path */
		
		LoadDoc(fileOpen.getSelectedFile());
	}

	
	@SuppressWarnings("unchecked")		//unavoidable Object->Collection
	public static boolean LoadDoc(File myFile) {
		try 
		{
			if(myFile instanceof File)	//prevent NullPointException on Cancel
			{
				FileInputStream fileIn = new FileInputStream(myFile);
				ObjectInputStream is = new ObjectInputStream(fileIn);
				ArrayList<DrawObject> inList;
				inList = (ArrayList<DrawObject>)is.readObject();
				ProgramElement.setMyList(inList);
				is.close();
			}//if
			
		}//try 
		
		catch (IOException | ClassNotFoundException ioErr) 
		{
			System.err.println(ioErr.getMessage());
			
			okResponseModal(DrawControl.myPanel,"Error","Failed to load file");
			
		}//catch
		
		DrawControl.myPanel.removeAll();
		DrawControl.myPanel.add(new MyCanvas());
		DrawControl.myPanel.setVisible(false);
		DrawControl.myPanel.setVisible(true);
		return false;
	}

	/**
	 * Open file selection dialog and pass result to second step of file save.
	 */
	public static void SaveSelection () {
		JFileChooser fileOpen = new JFileChooser();
		fileOpen.showOpenDialog(new JFrame("Select File"));
		SaveDoc(fileOpen.getSelectedFile());
	}

	public static boolean SaveDoc(File myFile) {
		try 
		{
			if(myFile instanceof File)	//prevent NullPointException on Cancel
			{
				FileOutputStream fileStream = new FileOutputStream(myFile);
				ObjectOutputStream os = new ObjectOutputStream(fileStream);
				os.writeObject(ProgramElement.getMyList());
				os.close();
			}//if
			
		}//try
		
		catch (IOException ioErr) 
		{
			System.err.println(ioErr.getMessage());
			
			okResponseModal(DrawControl.myPanel,"Error","Failed to save file");
			
		}//catch
		
		return false;
	}


	public static boolean ClearScreen() {
		ProgramElement newElement = new ProgramElement();
		newElement.deleteAll();
		DrawControl.myPanel.removeAll();
		DrawControl.myPanel.add(new MyCanvas());
		DrawControl.myPanel.setVisible(false);
		DrawControl.myPanel.setVisible(true);

		return false;
	}
	
	
	/** Exit PoorPaint normally */
	
	public static void ExitPoorPaint() 
	{
		System.exit(0);
		
	}//method
	
	
	/** Modal dialog with application version */
	
	public void ShowVersion()
	{
		okResponseModal(DrawControl.myPanel,"About: PoorPaint", "Version " +
				appInfo.VERSION.value());
		
	}//method
	
	
    /**
     * Modal dialog
     * 
     * @param titleIn  String Modal dialog title
     * @param msgIn  String message to show user
     */
    
	public static void okResponseModal(JPanel panelIn, String titleIn, 
			String msgIn)
	{		
		ImageIcon versionIcn = null;
		
		try 
		{
			versionIcn = ToolControl.createImageIcon(
					PoorPaint.iconSubFolder+ SystemControl.icons.
					appIconFileName.val(), "Version icon");
			
		} 
		catch (MalformedURLException iconErr) 
		{
			System.err.println(iconErr.getMessage());
			
		}//try

		JOptionPane.showMessageDialog(panelIn, msgIn, titleIn, JOptionPane.
				PLAIN_MESSAGE, versionIcn);

		return;
		
	}//method

}
