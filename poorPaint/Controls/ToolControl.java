/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import poorPaint.PoorPaint;
import poorPaint.Interfaces.ITools;

/**
 * Class ToolControl
 * 
 * Provides the control panel for the PoorPaint program.
 * The control panel lives in the WEST part of the frame.
 * 
 * Uses default provided constructor.  Does not initialize values.  Depends 
 * upon application generating first call in panel function for variable 
 * initialization.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @param RECTANGLE_NUM The constant value for the rectangle tool.
 * @param LINE_NUM The constant value for the line tool.
 * @param toolSelected The drawing function currently in use.
 * @see java.swing.JPanel
 */
public class ToolControl implements ITools {
	
	/* Constants */
	
	public static final int RECTANGLE_NUM = 0;
	public static final int LINE_NUM = 1;

	public static final int CIRCLE_NUM = 2;
	public static final int FREEHAND_NUM = 3;

	public static JLabel selToolLbl = new JLabel(); 
	
	/** Icon graphics for Tool buttons. */
	
	public static enum icons
	{
		fileNames ("line_35.jpeg","oval_35.jpeg","pen_35.jpeg",
				"rectangle_35.jpeg");
	
		String [] toolTipsTxt = {"Line","Hold <Shift> for Circle","Pen",
				"Hold <Shift> for Square"};
		
		Map<String,Integer> fileNamesMap = new HashMap<String,Integer>();
		
		int[] toolCodes = {LINE_NUM,CIRCLE_NUM,FREEHAND_NUM,RECTANGLE_NUM};
		
		String iconFileName = "";
		
		icons(String... fileNamesIn) 
		{ 
			for(int ct=0; ct< fileNamesIn.length; ct++)
			{
				fileNamesMap.put(PoorPaint.iconSubFolder+fileNamesIn[ct],
						toolCodes[ct]);

			}//for
			
		}//constructor
		
		
		/** @return Map  filenames and constant codes for icons, tools */
		
		Map<String,Integer> valueList()
		{
			return Collections.unmodifiableMap(fileNamesMap);
			
		}//method
				
		
		/**
		 * Retrieve icon file name for given tool number.
		 * 
		 * @param toolCodeIn  Integer of tool number
		 * 
		 * @return String  file name of icon graphic for given tool number 
		 */
		
		public String iconFileName(Integer toolCodeIn)
		{
			if( fileNamesMap.containsValue(toolCodeIn) )
			{
				for(String fileNmIn : fileNamesMap.keySet() )
				{
					if( fileNamesMap.get(fileNmIn) == toolCodeIn )
						return fileNmIn;
					
				}//for
				
			}//if
			
			return "Not found";
			
		}//method
		
	
		/**
		 * Retrieve tool tip text for a tool code.
		 * 
		 * @param toolCodeIn  Integer tool number identifier
		 * 
		 * @return  String tool tip text for given tool number
		 */
		
		public String toolTip(Integer toolCodeIn)
		{
			for(int ct=0; ct<toolCodes.length; ct++)
			{
				if( toolCodes[ct] == toolCodeIn )
					return toolTipsTxt[ct];

			}//for
			
			return "";
			
		}//method
		
	}//enum
	
	
    /* Variables */
	private static ArrayList<JButton> toolButtons = new ArrayList<JButton>();
	public static int selectedTool; 

	/** Result of modal confirmation prompt for downloading icons */
	
	private static int downloadIcons = Integer.MIN_VALUE;
	
	/** Stroke size control */
	
	public static JSpinner brushSizeCtrl = new JSpinner();
	
	/**
	 * Empty class initiator which does not reset variables.
	 */
	public ToolControl () {}
	
	/**
	 * Class initiator designed to set initial variable values.
	 */
	public ToolControl (JPanel whichOne) {

		toolButtons.clear();
		
		createTool("Rectangle", RECTANGLE_NUM);
		createTool("Line", LINE_NUM);
		
		createTool("Oval", CIRCLE_NUM);
		createTool("Pen", FREEHAND_NUM);
		
		selectedTool = RECTANGLE_NUM;
		
		whichOne.add(toolButtons.get(RECTANGLE_NUM));
		whichOne.add(toolButtons.get(CIRCLE_NUM));		
		whichOne.add(toolButtons.get(LINE_NUM));
		whichOne.add(toolButtons.get(FREEHAND_NUM));
		
		whichOne.add(new JLabel(" "));	/** TODO crude spacers in layout */
		
		selToolLbl.setText(returnName());
		selToolLbl.setFont(new Font("sanserif", Font.PLAIN, 12));
		
		//whichOne.add(selToolLbl);
		
		brushSizeCtrl = new JSpinner(new SpinnerNumberModel(1,1,5,1));
		
		brushSizeCtrl.setToolTipText("Size");
		brushSizeCtrl.setOpaque(false);
		
		brushSizeCtrl.setAlignmentX(toolButtons.get(
				FREEHAND_NUM).getAlignmentX());

		brushSizeCtrl.setMaximumSize(new Dimension(40,20));
		brushSizeCtrl.setSize(new Dimension(20,20));
		
		whichOne.add(new JLabel(" "));
		
		whichOne.add(brushSizeCtrl);
		
	}

	/**
	 * Returns the name of currently selected tool.
	 */
	@Override
	public String returnName() {
		String myName;

		switch(selectedTool) {
		case RECTANGLE_NUM:
			myName = "Rectangle";
			break;
		case LINE_NUM:
			myName = "Line";
			break;
		case CIRCLE_NUM:
			myName = "Oval";
			break;
		case FREEHAND_NUM:
			myName = "Pen";
			break;
        default:
        	myName = "";
        	break;
		}		
		return myName;
	}

	/**
	 * Starts new drawing object.  Wipes current coordinates and places 
	 * starting point.
	 */
	@Override
	public boolean pointStart(Point myPoint) {
		return true;
	}

	/**
	 * Continues drawing object.  Adds to existing array list.
	 */
	@Override
	public boolean pointAdd(Point myPoint) {
		return true;
	}

	/**
	 * Ends new drawing object.  Places last point and calls draw function.
	 */
	@Override
	public boolean pointEnd(Point myPoint) {
		return true;
	}

	@Override
	public void createTool(String myName, int myFunction) 
	{
		JButton myButton = new JButton();	
		
		/** TODO need null objects instead */
		
		ImageIcon iconIn = null;
		
		/* attempt to load icons, default to text buttons if unavailable */
		
		try
		{
			iconIn = createImageIcon(icons.fileNames.iconFileName(myFunction),
					myName);

		}
		catch(MalformedURLException iconLoadErr)
		{
			System.err.println(iconLoadErr.getMessage());
			
		}//try

		if(iconIn != null)
		{
			myButton.setIcon(iconIn);
		
		}
		else
		{
			myButton = new JButton(myName);
			
		}//if

		myButton.setContentAreaFilled(false);
		myButton.setMargin(new Insets(0,0,0,0));

		if(myFunction==selectedTool)
		{
			myButton.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(Color.BLACK,1),
					BorderFactory.createLoweredSoftBevelBorder()));
			
		}
		else
		{
			myButton.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(Color.BLACK,0),
					BorderFactory.createRaisedSoftBevelBorder()));
			
		}//if
		
		myButton.setName(myName);
		
		myButton.setHorizontalTextPosition(SwingConstants.CENTER);
		
		myButton.setToolTipText(icons.fileNames.toolTip(myFunction));
		
		selectTool(myFunction);
		myButton.addActionListener(new ToolListener());

		toolButtons.add(myFunction, myButton);		
	}

	@Override
	public void selectTool(int myTool) {
		selectedTool = myTool;
		
		selToolLbl.setText(returnName());
		
	}

	/**
	 * Class ToolListener
	 * 
	 * Inner class providing the listening service for the toolbar.
	 * 
	 * @version 1.0 March 18 2013
	 * @author Shane Milam
	 */
	public class ToolListener implements ActionListener {
		public int myTool;
		
		/**
		 * Initiate tool listening class and link the button to the listener
		 */
		public ToolListener () {
			myTool = selectedTool;
		}
		
		public void actionPerformed(ActionEvent myEvent) {
			selectTool (myTool);
			
			JButton selToolButton = (JButton)myEvent.getSource();

			/* Reset appearance of all tool buttons */
			
			for(Component comIn : selToolButton.getParent().getComponents())
			{
				if( comIn.getName() != null 
						&& ( comIn.getName().equalsIgnoreCase("Rectangle") 
						 || comIn.getName().equalsIgnoreCase("Line") 
						 || comIn.getName().equalsIgnoreCase("Pen") 
						 || comIn.getName().equalsIgnoreCase("Oval") ) )
				{
					JButton comInBtn = (JButton)comIn;
					
					comInBtn.setBorder(
							BorderFactory.createCompoundBorder(
							BorderFactory.createLineBorder(Color.BLACK,0),
							BorderFactory.createRaisedSoftBevelBorder()));

				}//if
				
			}//for

			/* Change appearance, border of selected tool */
			
			selToolButton.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(Color.BLACK,1),
					BorderFactory.createLoweredSoftBevelBorder()));

			
		}//method
	}
	
	
	/** 
	 * Returns an ImageIcon, or uninitialized ImageIcon reference path 
	 * invalid.<p>
	 * 
	 * Offers user choice to download icons
	 * 
	 * @param pathIn  String icon filename in 'images/' sub directory
	 * @param descIn  String icon description
	 * 
	 * @throws MalformedURLException  icon could not be located
	 *  
	 * @see http://docs.oracle.com/javase/tutorial/uiswing/components/icon.html 
	 */
	
	public static ImageIcon createImageIcon(String pathIn, String descIn) 
			throws MalformedURLException 
	{
		URL imgURL = ToolControl.class.getResource(pathIn);
		
		if(imgURL != null)
	    {
	    	return new ImageIcon(imgURL, descIn);
	    	
	    }
	    else
	    {	
	    	if(downloadIcons == Integer.MIN_VALUE)
	    	{
	    		downloadIcons = JOptionPane.showConfirmDialog(new JPanel(), 
	    				"Icons not found. Download now?");
	    		
	    	}//if

    		if(downloadIcons!=JOptionPane.YES_OPTION)
    		{	
				throw new MalformedURLException("Unable retrieve: "+pathIn);
    			
    		}//if
    		

	    	URL imgNetURL = new URL(SystemControl.icons.
	    			iconAuxillaryArchive.val()+pathIn);
	    	
	    	try
	    	{

	    		BufferedImage imgDwnld = ImageIO.read(imgNetURL);
	    		
	    		if(imgDwnld != null)
	    		{
	    			return new ImageIcon(imgDwnld);
	    			
	    		}//if

				throw new MalformedURLException("Unable retrieve: " + 
						SystemControl.icons.iconAuxillaryArchive.val()+pathIn);

	    	}
	    	catch(IOException | SecurityException dwnldErr)
	    	{
	    		System.err.println("Download failed " + imgNetURL.toString());

				throw new MalformedURLException("Unable retrieve: " + 
						SystemControl.icons.iconAuxillaryArchive.val());

	    	}//try
	    				
	    }//if
	    
	}//method
	
}