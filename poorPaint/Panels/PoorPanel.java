/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */
 
package poorPaint.Panels;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import poorPaint.Controls.SystemControl;

/**
 * Class PoorPanel
 * Provides an outline for each of the core program panel types.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @param CONTROL_NUM Constant value for the control panel.
 * @param APP_NUM Constant value for the app panel.
 * @param COLOR_NUM Constant value for the color panel.
 * @param SYSTEM_NUM Constant value for the system panel.
 * @param DRAWING_NUM Constant value for the drawing panel.
 * @param controlPart The control panel portion of the application frame.
 *        Used for displaying and selecting the drawing controls.
 * @param appPart The app panel portion of the application frame.
 *        Used for displaying application specific information.
 * @param colorPart The color panel portion of the application frame.
 *        Used for displaying and selecting the drawing color.
 * @param systemPart The system panel portion of the application frame.
 *        Used for displaying and handling the file/system operations.
 * @param drawingPart The drawing panel portion of the application frame.
 *        Used as the drawing canvas for the program.
 * @param poorPanels Array list of all application panels.
 * @param poorFrame This parent application frame placed so panels have access.
 * @see java.swing.JPanel
 */
public class PoorPanel extends JPanel {

	private static final long serialVersionUID = (long) SystemControl.appInfo.
			VERSION.value();
	
	/* Constants */
	public final int CONTROL_NUM = 0;
	public final int APP_NUM = 1;
	public final int COLOR_NUM = 2;
	public final int SYSTEM_NUM = 3;
	public final int DRAWING_NUM = 4;
	
    /* Variables */
	private ArrayList<JPanel> poorPanels = new ArrayList<JPanel>();
	private ControlPanel controlPart;
	private AppPanel appPart;
	private ColorPanel colorPart;
	private SystemPanel systemPart;
	private DrawingPanel drawingPart;
	private JFrame poorFrame;
	
	/**
	 * Instantiate a new PoorPanel object.
	 * Overloaded constructor only--no empty constructor allowed.
	 */
	public PoorPanel(JFrame whichOne) {
	    /* Variables */
		setPoorFrame(whichOne);

		/* Initialize Panels */
		controlPart = new ControlPanel();
		appPart = new AppPanel();
		colorPart = new ColorPanel();
		systemPart = new SystemPanel();
		drawingPart = new DrawingPanel();
		
		/* Assemble Panels */
		poorPanels.add (CONTROL_NUM, controlPart);
		poorPanels.add (APP_NUM, appPart);
		poorPanels.add (COLOR_NUM, colorPart);
		poorPanels.add (SYSTEM_NUM, systemPart);
		poorPanels.add (DRAWING_NUM, drawingPart);		
	}

	/**
	 * Return a panel to caller.
	 */
	public JPanel ReturnPanel(int whichOne) {
	    /* Variables */
		JPanel whichFrame;

		switch(whichOne) {
		case CONTROL_NUM:
			whichFrame = ((ControlPanel)poorPanels.get(CONTROL_NUM)).
					ReturnPanel();
			break;
		case APP_NUM:
			whichFrame = ((AppPanel)poorPanels.get(APP_NUM)).
					ReturnPanel();
			break;
		case COLOR_NUM:
			whichFrame = ((ColorPanel)poorPanels.get(COLOR_NUM)).
					ReturnPanel();
			break;
		case SYSTEM_NUM:
			whichFrame = ((SystemPanel)poorPanels.get(SYSTEM_NUM)).
					ReturnPanel();
			break;
		case DRAWING_NUM:
			whichFrame = ((DrawingPanel)poorPanels.get(DRAWING_NUM)).
					ReturnPanel();
			break;
        default:
        	whichFrame = null;
        	break;
		}
		return whichFrame;
	}

	/**
	 * Get a private variable - poorFrame.
	 */
	public JFrame getPoorFrame() {
		return poorFrame;
	}

	/**
	 * Set a private variable - poorFrame.
	 */
	public void setPoorFrame(JFrame poorFrame) {
		this.poorFrame = poorFrame;
	}
}