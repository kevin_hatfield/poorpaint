/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics 
 *   identified for development of Project 2 in Object-Oriented Design I SSE 
 *   550 Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.net.MalformedURLException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JLabel;

import poorPaint.Controls.SystemControl;
import poorPaint.Controls.ToolControl;

/**
 * Class AppPanel
 * 
 * Provides the app panel for the PoorPaint program.
 * The app panel lives in the NORTH part of the frame.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @param mainMessage Primary string to show in panel.
 * @param myPanel The actual graphic panel for AppPanel.
 * @see java.swing.JPanel
 */
public class AppPanel extends JPanel {
    
	private static final long serialVersionUID = (long) SystemControl.appInfo.
			VERSION.value();
	
	/* Variables */
	private String mainMessage = "Poor Paint Program";
	
	private JPanel myPanel;

	/**
	 * Instantiate a new AppPanel object.
	 */
	public AppPanel() {
	    /* Variables */
		Font controlFont = new Font("sanserif", Font.BOLD, 24);
		JLabel controlDisplay = new JLabel();

		controlDisplay.setFont(controlFont);
		controlDisplay.setText(mainMessage);

		Font menuFont = new Font("sanserif", Font.PLAIN, 12);
		Font subMenuFont = new Font("sanserif", Font.PLAIN, 11);		

		/* Help, About version */
		
		JMenuBar hlpMenuBar = new JMenuBar();
				
		JMenu hlpMenu = new JMenu("Help");

		hlpMenu.setFont(menuFont);
		
		hlpMenu.setMnemonic(KeyEvent.VK_H);
		
		JMenuItem hlpMenuItem = new JMenuItem("About");
		
		hlpMenuItem.setFont(subMenuFont);
		
		hlpMenuItem.setMnemonic(KeyEvent.VK_A);
		
		hlpMenuItem.addActionListener(new AboutMenuMouseDrive());
		
		hlpMenu.add(hlpMenuItem);
		
		hlpMenuBar.add(hlpMenu);

		/* File Open, Save */

		JMenuBar fileMenuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		
		fileMenu.setFont(menuFont);
		fileMenu.setMnemonic(KeyEvent.VK_F);

		JMenuItem fileOpenMenuItem = new JMenuItem("Open...");
		fileOpenMenuItem.setMnemonic(KeyEvent.VK_O);
		
		JMenuItem fileSaveMenuItem = new JMenuItem("Save");
		fileSaveMenuItem.setMnemonic(KeyEvent.VK_S);
		
		JMenuItem fileQuitMenuItem = new JMenuItem("Quit");
		fileQuitMenuItem.setMnemonic(KeyEvent.VK_Q);
		
		fileOpenMenuItem.setFont(subMenuFont);
		fileSaveMenuItem.setFont(subMenuFont);
		fileQuitMenuItem.setFont(subMenuFont);
		
		JMenuBar editMenuBar = new JMenuBar();
		
		JMenu editMenu = new JMenu("Edit");
		editMenu.setFont(menuFont);
		editMenu.setMnemonic(KeyEvent.VK_E);
		
		JMenuItem clearEditMenuItem = new JMenuItem("Clear");
		clearEditMenuItem.setFont(subMenuFont);
		clearEditMenuItem.setMnemonic(KeyEvent.VK_R);
		
		/* attempt to load icons, default to text menus */
		
		try 
		{
			ImageIcon openIcon = ToolControl.createImageIcon(
					 SystemControl.icons.fileNames.iconFileName(SystemControl.
							 LOAD_NUM),"Open");
			
			fileOpenMenuItem.setIcon(openIcon);

			ImageIcon saveIcon = ToolControl.createImageIcon(
					 SystemControl.icons.fileNames.iconFileName(SystemControl.
							 SAVE_NUM),"Save");
			
			fileSaveMenuItem.setIcon(saveIcon);

			ImageIcon quitIcon = ToolControl.createImageIcon(
					 SystemControl.icons.fileNames.iconFileName(SystemControl.
							 QUIT_NUM),"Quit");
			
			fileQuitMenuItem.setIcon(quitIcon);

			ImageIcon clearIcon = ToolControl.createImageIcon(
					 SystemControl.icons.fileNames.iconFileName(SystemControl.
							 ERASE_NUM),"Clear");
			
			clearEditMenuItem.setIcon(clearIcon);
			
		}
		catch (MalformedURLException iconLoadErr) 
		{
			System.err.println(iconLoadErr.getMessage());
			
		}//try
		
		fileOpenMenuItem.addActionListener(new OpenMenuMouseDrive());
		fileSaveMenuItem.addActionListener(new SaveMenuMouseDrive());
		fileQuitMenuItem.addActionListener(new QuitMenuMouseDrive());
		
		fileMenu.add(fileOpenMenuItem);
		fileMenu.add(fileSaveMenuItem);
		fileMenu.add(fileQuitMenuItem);
		
		fileMenuBar.add(fileMenu);
		
		clearEditMenuItem.addActionListener(new ClearMenuMouseDrive());
		
		editMenu.add(clearEditMenuItem);
		editMenuBar.add(editMenu);

		/* layout: manual, no window builder usage */		
		
		myPanel = new JPanel();
		myPanel.setLayout(new BoxLayout(myPanel,BoxLayout.X_AXIS));

		Dimension maxMenuBar = new Dimension(20,20);
		
		fileMenuBar.setMaximumSize(maxMenuBar);
		
		editMenuBar.setMaximumSize(maxMenuBar);
		
		myPanel.add(fileMenuBar);
		
		myPanel.add(Box.createRigidArea(new Dimension(1, 0)));
		
		myPanel.add(editMenuBar);
		
		JPanel titlePanel = new JPanel(new FlowLayout());

		titlePanel.add(controlDisplay);
		titlePanel.setOpaque(false);
		titlePanel.setAlignmentY(Component.TOP_ALIGNMENT);
		hlpMenuBar.setMaximumSize(maxMenuBar);
		myPanel.add(titlePanel);
		
		myPanel.add(hlpMenuBar);
		
		myPanel.setBackground(Color.lightGray);
		
	}
	
	
	/**
	 * Return the panel ID back to outer class caller.
	 */
    public JPanel ReturnPanel() {
    	return (myPanel);
    }

    /** Handle mouse click of Help/About submenu item */
    
	class AboutMenuMouseDrive extends MouseAdapter implements ActionListener
	{
		public void mouseClicked(ActionEvent arg0) 
		{
			new SystemControl(myPanel).ShowVersion();
			
		}//method

		@Override
		public void actionPerformed(ActionEvent arg0) {	mouseClicked(arg0);	}
		
	}//inner class

	
    /** Handle mouse click of File/Save submenu item */
    
	class SaveMenuMouseDrive extends MouseAdapter implements ActionListener
	{
		public void mouseClicked(ActionEvent arg0) 
		{
			SystemControl.selectedSystem = SystemControl.SAVE_NUM;
			
			SystemControl.SaveSelection();
			
		}//method

		@Override
		public void actionPerformed(ActionEvent arg0) {	mouseClicked(arg0);	}
		
	}//inner class

	
    /** Handle mouse click of File/Open... submenu item */
    
	class OpenMenuMouseDrive extends MouseAdapter implements ActionListener
	{
		public void mouseClicked(ActionEvent arg0) 
		{
			SystemControl.selectedSystem = SystemControl.LOAD_NUM;
			
			SystemControl.LoadSelection();
			
		}//method

		@Override
		public void actionPerformed(ActionEvent arg0) {	mouseClicked(arg0);	}
		
	}//inner class


    /** Handle mouse click of File/Quit submenu item */
    
	class QuitMenuMouseDrive extends MouseAdapter implements ActionListener
	{
		public void mouseClicked(ActionEvent arg0) 
		{
			SystemControl.selectedSystem = SystemControl.QUIT_NUM;
			
			SystemControl.ExitPoorPaint();
			
		}//method

		@Override
		public void actionPerformed(ActionEvent arg0) {	mouseClicked(arg0);	}
		
	}//inner class

	
    /** Handle mouse click of Clear menu item */
    
	class ClearMenuMouseDrive extends MouseAdapter implements ActionListener
	{
		public void mouseClicked(ActionEvent arg0) 
		{
			SystemControl.selectedSystem = SystemControl.ERASE_NUM;
			
			SystemControl.ClearScreen();
			
		}//method

		@Override
		public void actionPerformed(ActionEvent arg0) {	mouseClicked(arg0);	}
		
	}//inner class
	
}