/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Panels;

import javax.swing.JPanel;
import javax.swing.BoxLayout;

import poorPaint.Controls.DrawControl;
//import poorPaint.Elements.ProgramElement;
import poorPaint.Controls.SystemControl;

/**
 * Class DrawingPanel
 * 
 * Provides the drawing panel for the PoorPaint program.
 * The control drawing lives in the CENTER part of the frame.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @param isDirty Indicates if content has changed and requires saving.
 * @param myPanel The actual graphic panel for AppPanel.
 * @see java.swing.JPanel
 */
public class DrawingPanel extends JPanel {

	private static final long serialVersionUID = (long) SystemControl.appInfo.
			VERSION.value();
	
	/* Variables */
    //private ProgramElement myElement;
	private JPanel myPanel;

	/**
	 * Instantiate a new AppPanel object.
	 */
	public DrawingPanel() {

		myPanel = new JPanel();
		myPanel.setLayout(new BoxLayout(myPanel, BoxLayout.Y_AXIS));
		
		//myElement = new ProgramElement();
		new DrawControl(myPanel);
	}
		
	/**
	 * Return the panel ID back to outer class caller.
	 */
    public JPanel ReturnPanel() {
    	return (myPanel);
    }	
}