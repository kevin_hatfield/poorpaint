/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics 
 *   identified for development of Project 2 in Object-Oriented Design I SSE 
 *   550 Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Panels;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.BoxLayout;

import poorPaint.Controls.ColorControl;
import poorPaint.Controls.SystemControl;

/**
 * Class ColorPanel
 * 
 * Provides the color panel for the PoorPaint program.
 * The color panel lives in the SOUTH part of the frame.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @param myPanel The actual graphic panel for AppPanel.
 * @see java.swing.JPanel
 */
public class ColorPanel extends JPanel {

	private static final long serialVersionUID = (long) SystemControl.appInfo.
			VERSION.value();
	
	/* Variables */
	private JPanel myPanel;

	/**
	 * Instantiate a new AppPanel object.
	 */
	public ColorPanel() {

		myPanel = new JPanel();
		myPanel.setBackground(Color.lightGray);
		myPanel.setLayout(new BoxLayout(myPanel, BoxLayout.X_AXIS));
		new ColorControl(myPanel);		
	}
	
	/**
	 * Return the panel ID back to outer class caller.
	 */
    public JPanel ReturnPanel() {
    	return (myPanel);
    }
}