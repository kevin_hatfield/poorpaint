/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Interfaces;

import java.awt.Point;

/**
 * Interface ITools
 * 
 * Framework for drawings tools housed  within ControlPanel.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 */
public interface ITools {
	String returnName();
	/* Establish first point for shape */
	boolean pointStart(Point myPoint);
	/* Enter a new point between the first and last */
	boolean pointAdd(Point myPoint);
	/* Enter the last point for a shape */
	boolean pointEnd(Point myPoint);
	/* Generate a new tool/button */
	void createTool(String myName, int myFunction);
	/* Select tool for use */
	void selectTool(int myTool);
}