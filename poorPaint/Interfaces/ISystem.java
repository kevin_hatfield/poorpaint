/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics 
 *   identified for development of Project 2 in Object-Oriented Design I SSE 
 *   550 Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */

package poorPaint.Interfaces;

import java.io.File;

/**
 * Interface ISystem
 * 
 * Deliver essential system functions (save, load).
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * 
 * @deprecated SystemControl contains static versions of methods
 */
public interface ISystem {	
	/* For Loading Files */
	boolean LoadDoc(File myFile);
	/* For Saving Files */
	boolean SaveDoc(File myFile);
	/* For Erasing All Work */
	boolean ClearScreen();
	/* For Closing Application */
	void ExitPoorPaint();
}