/**
 * PoorPaint
 * 
 * <p>
 *   <b>Description</b> :
 *   Very simple drawing program developed for demonstration of topics identified
 *   for development of Project 2 in Object-Oriented Design I SSE 550
 *   Mercer University - School of Engineering
 * </p>
 * <p>
 *   <b>History</b> :
 *   <ul>
 *     <li>Version 1.0 : Released for grading</li>
 *     <li>Version 0.1 : Development</li>
 *   </ul>
 * </p>
 * <p>
 *   <b>Copyright (c)</b> :
 *   <ul>
 *     <li>2013</li>
 *     <li>Shane Milam</li>
 *     <li>All Rights Reserved</li>
 *   </ul>
 * </p>
 * @version 1.0 March 18 2013
 * @see <a href="http://paulemacneil.com/syl550-13s.html">Course Syllabus</a>
 */
 
package poorPaint;

import java.awt.BorderLayout;
import java.awt.Color;
import java.net.MalformedURLException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import poorPaint.Controls.SystemControl;
import poorPaint.Controls.ToolControl;
import poorPaint.Panels.PoorPanel;

/**
 * Class PoorPaint
 * 
 * Provides framework for drawing program and main program function.
 * 
 * @version 1.0 March 18 2013
 * @author Shane Milam
 * @param serialVersionUID provides developer controlled UID for serialization.
 */
public class PoorPaint extends JFrame {
	
	public final static int wdwHeight = 768; 
	public final static int wdwWidth = 1024;
	
	private static final long serialVersionUID = (long) SystemControl.appInfo.
			VERSION.value();

	/** Icon graphics path */
	
	public static final String iconSubFolder = "/images/";

	
    /* Variables */
	private JFrame myFrame;
	private PoorPanel myPanel;
	
	/**
	 * Instantiate a new PoorPaint object.
	 */
	public PoorPaint() {
		myFrame = new JFrame();
		myFrame.getContentPane().setLayout(new BorderLayout());
		myPanel = new PoorPanel(myFrame);
		myFrame.setBackground(Color.white);
		myFrame.setSize(wdwWidth,wdwHeight);
		myFrame.setTitle("Poor Paint");
		myFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		myFrame.getContentPane().add(BorderLayout.WEST, myPanel.ReturnPanel(
				myPanel.CONTROL_NUM));
		
		myFrame.getContentPane().add(BorderLayout.NORTH, myPanel.ReturnPanel(
				myPanel.APP_NUM));
		
		myFrame.getContentPane().add(BorderLayout.SOUTH, myPanel.ReturnPanel(
				myPanel.COLOR_NUM));
		
		myFrame.getContentPane().add(BorderLayout.EAST, myPanel.ReturnPanel(
				myPanel.SYSTEM_NUM));
		
		myFrame.getContentPane().add(BorderLayout.CENTER, myPanel.ReturnPanel(
				myPanel.DRAWING_NUM));
	
		ImageIcon appIcon;
		try 
		{
			appIcon = ToolControl.createImageIcon(
					iconSubFolder + 
					SystemControl.icons.appIconFileName.val()
					, "app icon");
			
			myFrame.setIconImage(appIcon.getImage());

		} 
		catch (MalformedURLException iconMissingErr) 
		{
			System.err.println(iconMissingErr.getMessage());
		
		}//try
				
	}
	
	/**
	 * Program entry point.
	 * 
	 * @param args Command line arguments.
	 * @param projTwo Application instance.
	 */
	public static void main(String args[]) {
	    /* Variables */
		PoorPaint projTwo = new PoorPaint();

		projTwo.go();	
	}

	/**
	 * Primary program execution and termination.
	 * 
	 * @param myFrame Primary application frame.
	 * @param myPanel Application panel template.
	 */
	public void go() {
		myFrame.setVisible(true);

	}
	
	
}